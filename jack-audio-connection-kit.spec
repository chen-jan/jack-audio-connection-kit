%global groupname jackuser

Name:           jack-audio-connection-kit
Version:        1.9.14
Release:        3
Summary:        A professional sound server
License:        GPL-2.0 and GPL-2.0+ and LGPL-2.1+ and GPL-3.0+
URL:            http://github.com/jackaudio/jack2
Source0:        https://github.com/jackaudio/jack2/releases/download/v%{version}/jack2-%{version}.tar.gz
Patch0:         %{name}-doxygen.patch

BuildRequires:  alsa-lib-devel dbus-devel doxygen expat-devel gcc-c++ libffado-devel libsamplerate-devel libdb-devel < 6.0.20
BuildRequires:  libsndfile-devel ncurses-devel opus-devel pkgconfig python3 readline-devel
Requires(pre):  shadow-utils
Requires:       pam
Provides:       jack-audio-connection-kit-dbus = %{version}-%{release}
Obsoletes:      jack-audio-connection-kit-dbus < %{version}-%{release}

%description
JACK Audio Connection Kit is a professional sound server daemon that provides real-time,
low-latency connections for both audio and MIDI data between applications that use its API.

%package devel
Summary:        The libraries and headers for Jack
Requires:       jack-audio-connection-kit = %{version}-%{release}
Provides:       jack-audio-connection-kit-example-clients = %{version}-%{release}
Obsoletes:      jack-audio-connection-kit-example-clients < %{version}-%{release}

%description devel
The package contains the libraries and headers necessary for the Jack Audio Connection Kit.

%package_help

%prep
%autosetup -n jack2-%{version} -p1

%build
export CPPFLAGS="$RPM_OPT_FLAGS"
export LDFLAGS="$RPM_LD_FLAGS"
export PREFIX=%{_prefix}
python3 ./waf configure %{?_smp_mflags} --mandir=%{_mandir}/man1 --libdir=%{_libdir} --doxygen --dbus --db \
   --classic --firewire --alsa --clients 256 --ports-per-application=2048
python3 ./waf build %{?_smp_mflags} -v

%install
python3 ./waf --destdir=$RPM_BUILD_ROOT install

mv $RPM_BUILD_ROOT%{_datadir}/jack-audio-connection-kit/reference .

mv $RPM_BUILD_ROOT%{_bindir}/jack_rec $RPM_BUILD_ROOT%{_bindir}/jackrec

chmod 755 $RPM_BUILD_ROOT%{_libdir}/jack/*.so $RPM_BUILD_ROOT%{_libdir}/libjack*.so.*.*.*

%pre
getent group %groupname > /dev/null || groupadd -r %groupname
exit 0

%post
/sbin/ldconfig
%postun
/sbin/ldconfig

%files
%doc ChangeLog.rst
%{_bindir}/jackd
%{_bindir}/jackrec
%{_libdir}/jack/
%{_libdir}/*.so.*
%{_bindir}/jackdbus
%{_datadir}/dbus-1/services/org.jackaudio.service
%{_bindir}/jack_control
%exclude %{_datadir}/jack-audio-connection-kit

%files devel
%{_includedir}/jack/
%{_libdir}/libjack.so
%{_libdir}/libjacknet.so
%{_libdir}/libjackserver.so
%{_libdir}/pkgconfig/jack.pc
%{_bindir}/*
%exclude %{_bindir}/jackd
%exclude %{_bindir}/jackrec
%exclude %{_bindir}/jackdbus
%exclude %{_bindir}/jack_control

%files help
%doc README.rst README_NETJACK2
%doc reference/html/
%{_mandir}/man1/*
%exclude %{_mandir}/man1/jack_impulse_grabber.1*

%changelog
* Fri Oct 22 2021 Ge Wang <wangge20@huawei.com> - 1.9.14-3
- Fix groupname is not a valid name error

* Wed Jan 20 2021 Ge Wang <wangge20@huawei.com> - 1.9.14-2
- Modify license information.

* Thu Nov 12 2020 leiju<leiju4@huawei.com> - 1.9.14-1
- Update to 1.9.14

* Mon Feb 24 2020 xuxijian<xuxijian@huawei.com> - 1.9.12-9
- Package init
